/**
 * @file   life.cpp
 * @author Gleb Semenov <gleb.semenov@gmail.com>
 * @date   Thu May  9 20:21:01 2019
 *
 * @brief  The game of life
 *
 *
 */

#include "life.hpp"
#include <cstdio>  /* for printf */
#include <cstdlib> /* for exit */
#include <getopt.h>

static struct option long_options[] = {{"geometry", optional_argument, 0, 'g'},
                                       {"born", optional_argument, 0, 'b'},
                                       {"survive", optional_argument, 0, 's'},
                                       {"help", no_argument, 0, 'h'},
                                       {0, 0, 0, 0}};

static const char *help_message =
    "The life game demo\n"
    "Options:\n"
    "--geometry, -g plain|vcyl|hcyl|tor\n"
    "  set world's geometry, optional, default plain\n"
    "--born, -b [0-8]+\n"
    "  define number set of neighbors for a cell to be born\n"
    "  optional, default 3 (Conway's rule)\n"
    "--survive, -s [0-8]+\n"
    "  define number set of neighbors for a cell to survive\n"
    "  optional, default 23 (Conway's rule)\n"
    "--help, -h, -? - display this message\n";

static const char *survive_default = "23";
static const char *born_default = "3";

int get_geomflags(const char *arg) {
    static const char *geomvals[] = {"plain", "vcyl", "hcyl", "tor"};
    static const int geomflags[] = {0, VCYL, HCYL, VCYL | HCYL};
    int i;

    for (i = 0; i < sizeof(geomvals) / sizeof(geomvals[0]); i++)
        if (strncasecmp(arg, geomvals[i], strlen(geomvals[i])) == 0)
            break;

    if (i < sizeof(geomvals) / sizeof(geomvals[0]))
        return geomflags[i];
    else
        return -1;
}

std::string check_number_string(const char *arg) {
    const char *p = arg;
    while (*p) {
        if (*p < '0' || *p > MAXNEIGHBORS + '0')
            return std::string("");
        p++;
    }
    return std::string(arg);
}

int main(int ac, char **av) {
    int geom = 0;
    std::string born;
    std::string survive;
    bool param_error = false;

    while (1) {
        int option_index = 0;
        int c = getopt_long(ac, av, "g:b:s:h?", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
        case 'g':
            geom = get_geomflags(optarg);
            if (geom < 0)
                param_error = true;
            break;
        case 'b':
            born = check_number_string(optarg);
            if (born.length() == 0)
                param_error = true;
            break;
        case 's':
            survive = check_number_string(optarg);
            if (survive.length() == 0)
                param_error = true;
            break;
        case 'h':
        case '?':
            std::cout << help_message;
            return 0;
        default:
            param_error = true;
            break;
        }
        if (param_error) {
            std::cout << "Invalid option!" << std::endl;
            std::cout << help_message;
            return EINVAL;
        }
    }

    if (survive.length() == 0)
        survive = survive_default;
    if (born.length() == 0)
        born = born_default;

    try {
        World<Drawer, NCurses> W(born.c_str(), survive.c_str(), geom);
        W.fillRandom();
        while (1) {
            getch();
            W.nextGeneration();
        }
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EFAULT;
    }

    return 0;
}
