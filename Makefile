ifdef DEBUG
EXTRAFLAGS = -g
else
EXTRAFLAGS = -O2
endif

LIBS = -lncurses -lstdc++
CPPFLAGS = $(EXTRAFLAGS) -std=c++20  -I. -Wall -pedantic $(LIBS) 
CXX = clang

SRC = life.cpp life.hpp

UNAME := $(shell uname)

all: life

ifeq ($(UNAME), Darwin)

x86 = x86_64-apple-macos10.12
arm64 = arm64-apple-macos11

life: life_$(x86) life_$(arm64)
	lipo -create -output $@ $^
life_$(x86): $(SRC)
	$(CXX) -o $@ $< $(CPPFLAGS) -target $(x86)
life_$(arm64): $(SRC)
	$(CXX) -o $@ $< $(CPPFLAGS) -target $(arm64)

else

life: $(SRC)
	$(CXX) -o $@ $< $(CPPFLAGS)

endif

clean:
	rm -rf *.o *~ core* *.dSYM life_*

distclean: clean
	rm -f life





