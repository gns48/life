/**
 * @file   life.hpp
 * @author Gleb Semenov <gleb.semenov@gmail.com>
 * @date   Sat May 11 00:18:41 2019
 *
 * @brief  The game of life, top-level header file
 *
 *
 */

#ifndef __LIFE_HPP__
#define __LIFE_HPP__

// current elementary cell type
typedef bool cell_unit_t;

#include <array>
#include <iostream>
#include <ncurses.h>
#include <random>
#include <stdexcept>
#include <string.h>
#include <utility>
#include <vector>
#include <type_traits>

#define MAXNEIGHBORS 8
// World's geometry
#define HCYL 1
#define VCYL 2

struct Drawer {
    Drawer(){};
    virtual ~Drawer(){};
    virtual int rows() = 0;
    virtual int cols() = 0;
    virtual void Refresh() = 0;
    virtual void drawBool(int r, int c, bool val) = 0;
};

class NCurses : public Drawer {
    int m_rows;
    int m_cols;

  public:
    NCurses() {
        initscr();
        noecho();
        clear();
        refresh();
        getmaxyx(stdscr, m_rows, m_cols);
    }

    virtual ~NCurses() { endwin(); }

    virtual int rows() { return m_rows; }
    virtual int cols() { return m_cols; }
    virtual void Refresh() { refresh(); }
    virtual void drawBool(int r, int c, bool val) {
        mvaddch(r, c, val ? '*' : '\x20');
    }
};

class Rule {
    std::array<bool, MAXNEIGHBORS + 1> m_survive;
    std::array<bool, MAXNEIGHBORS + 1> m_born;

  public:
    Rule() = delete;
    Rule(const Rule &) = delete;
    Rule(Rule &&) = delete;

    Rule(const char *bornCounts, const char *surviveCounts) {
        std::fill(m_survive.begin(), m_survive.end(), false);
        std::fill(m_born.begin(), m_born.end(), false);

        const char *p = bornCounts;
        while (*p)
            m_born[*p++ - '0'] = true;
        p = surviveCounts;
        while (*p)
            m_survive[*p++ - '0'] = true;
    }

    inline bool is_survive(int neighbors) { return m_survive[neighbors]; }
    inline bool is_born(int neighbors) { return m_born[neighbors]; }
};

template <typename T> using Board = std::vector<std::vector<T>>;

template<class BaseDrawer, class ObjDrawer>
	requires std::is_base_of_v<BaseDrawer, ObjDrawer> &&
		 std::is_abstract_v<BaseDrawer>
class World : public Rule {
    BaseDrawer *D;
    int m_geom;
    Board<bool> m_curr, *p_curr;
    Board<bool> m_next, *p_next;

    int neighborhood(int rw, int cl) {
        int ncount = 0;
        int r, c;

        for (int row = rw - 1; row <= rw + 1; row++) {
            if (row < 0) {
                if (m_geom & HCYL)
                    r = D->rows() - 1;
                else
                    continue;
            } else if (row == D->rows()) {
                if (m_geom & HCYL)
                    r = 0;
                else
                    continue;
            } else
                r = row;

            for (int col = cl - 1; col <= cl + 1; col++) {
                if (row == rw && col == cl)
                    continue;
                if (col < 0) {
                    if (m_geom & VCYL)
                        c = D->cols() - 1;
                    else
                        continue;
                } else if (col == D->cols()) {
                    if (m_geom & VCYL)
                        c = 0;
                    else
                        continue;
                } else
                    c = col;

                if ((*p_curr)[r][c])
                    ncount++;
            }
        }
        return ncount;
    }

  public:
    World() = delete;
    World(const World &) = delete;
    World(World &&) = delete;

    World(const char *bornCounts, const char *surviveCounts, int geom_flags)
        : Rule(bornCounts, surviveCounts), D(new ObjDrawer), m_geom(geom_flags) {
        for (int r = 0; r < D->rows(); r++) {
            std::vector<bool> vc, vn;
            vc.resize(D->cols());
            std::fill(vc.begin(), vc.end(), false);
            m_curr.push_back(vc);
            vn.resize(D->cols());
            std::fill(vn.begin(), vn.end(), false);
            m_next.push_back(vn);
        }
        p_curr = &m_curr;
        p_next = &m_next;
    }

    ~World() { delete D; };

    /**
     * evaluate next generation and swap pointers.
     *
     */
    void nextGeneration() {
        for (int r = 0; r < D->rows(); r++) {
            for (int c = 0; c < D->cols(); c++) {
                auto n = neighborhood(r, c);
                (*p_next)[r][c] =
                    ((*p_curr)[r][c] && is_survive(n)) || is_born(n);
                D->drawBool(r, c, (*p_next)[r][c]);
            }
        }
        D->Refresh();
        // swap Generations
        std::swap(p_curr, p_next);
    }

    /**
     * Create random first generation
     *
     */
    void fillRandom() {
        std::random_device rd;
        constexpr unsigned int false_limit =
            std::numeric_limits<unsigned int>::max() / 3;
        for (int r = 0; r < D->rows(); r++) {
            for (int c = 0; c < D->cols(); c++) {
                (*p_curr)[r][c] = (rd() >= false_limit);
                D->drawBool(r, c, (*p_curr)[r][c]);
            }
        }
        D->Refresh();
    }
};

#endif // #ifndef __LIFE_HPP__
